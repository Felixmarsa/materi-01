describe('empty spec', () => {
    it('Change image signature', () =>{
        cy.visit('app.privy.id')
        cy.get('input[name="user[privyId]"]').type('FMR8150', {
          force: true
      })
        cy.contains('CONTINUE').click()
        cy.get('input[name="user[secret]"]').type('Megasus123', {
          force: true
      })
        cy.contains('CONTINUE').click()
        cy.contains('Change My Signature Image').click()
        cy.contains('Add Signature').click()
        cy.get('input[id="name-initial"]').clear()
        cy.get('input[id="name-initial"]').type('Felix')
        cy.contains('Save').click()
      });
    
  })