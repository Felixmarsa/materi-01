describe('Sign and Share', () => {
    it('Sign and Share', () => {
        // visit PrivyId login page
        cy.visit('https://app.privy.id')
    
        // input username
        cy.get('input[name="user[privyId]"]').type('FMR8150', { force: true })
        cy.contains('CONTINUE').click()
    
        //input user password
        cy.get('input[name="user[secret]"]').type('Megasus123', { force: true })
        cy.contains('CONTINUE').click()
    
        // upload document
        cy.get('button[id="v-step-0"]').click()
        cy.contains('Self Sign').click()
        cy.contains('browse').click()
    
        const filepath = 'Test.pdf'
        cy.get('input[type="file"]').attachFile(filepath)
        
        cy.get('.modal-content .modal-footer button:contains("Upload")').click()
        
        cy.wait(2000)
        cy.contains('Continue').click()
        cy.wait(5000)
        cy.get('button[class="btn btn-danger"]').click()
        cy.contains('Done').click()
      });
    

})