describe('Basic Tests', () => {
  it('Self Sign', () => {
    // visit PrivyId login page
    cy.visit('https://app.privy.id')

    // input username
    cy.get('input[name="user[privyId]"]').type('FMR8150', { force: true })
    cy.contains('CONTINUE').click()

    //input user password
    cy.get('input[name="user[secret]"]').type('Megasus123', { force: true })
    cy.contains('CONTINUE').click()
  });
});
